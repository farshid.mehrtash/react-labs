# Mastering React Hooks with TypeScript

## Introduction

This lab focuses on mastering React Hooks within the context of TypeScript. Hooks are a feature in React that allows you to use state and other React features without writing a class. TypeScript enhances the development experience by providing type safety and improving code quality and maintainability.

### Objectives

- Learn how to use common React Hooks (`useState`, `useEffect`) with TypeScript.
- Understand how to type custom hooks for increased reliability.

### Prerequisites

- Basic understanding of React and TypeScript.
- Node.js installed on your computer.
- A code editor, such as Visual Studio Code.

## Step 1: Setting Up Your Project

1. If not already set up, create a new React project with TypeScript:

```bash
npx create-react-app my-hooks-lab --template typescript
cd my-hooks-lab
```

2. Open the project in your code editor.

## Step 2: Using `useState` and `useEffect` Hooks with TypeScript

### Creating a Counter Component

1. Create a new file named `Counter.tsx` in the `src` folder.

2. Define a functional component that uses the `useState` and `useEffect` hooks. Here's how you can type the state with TypeScript:

```typescript
import React, { useState, useEffect } from 'react';

const Counter: React.FC = () => {
  const [count, setCount] = useState<number>(0);

  useEffect(() => {
    document.title = `Count: ${count}`;
  }, [count]);

  return (
    <div>
      <h2>{count}</h2>
      <button onClick={() => setCount(count + 1)}>Increment</button>
    </div>
  );
};

export default Counter;
```

This component:
- Uses `useState` to track the counter's state, with an initial state of `0`.
- Uses `useEffect` to update the document's title whenever the `count` changes.

### Creating a Custom Hook

1. Define a custom hook `useCounter.ts` to abstract the counter logic:

```typescript
import { useState } from 'react';

export const useCounter = (initialValue: number = 0) => {
  const [count, setCount] = useState<number>(initialValue);
  const increment = () => setCount(count + 1);
  const decrement = () => setCount(count - 1);

  return { count, increment, decrement };
};
```

2. Use the `useCounter` hook in a component:

```typescript
import React from 'react';
import {useCounter} from './useCounter';

const CounterWithCustomHook: React.FC = () => {
    const {count, increment, decrement} = useCounter();

    return (
        <div>
            <h2>{count} < /h2>
        < button
    onClick = {increment} > Increment < /button>
        < button
    onClick = {decrement} > Decrement < /button>
        < /div>
)
    ;
};
```

## Conclusion

Through this lab, you've seen how to effectively use React Hooks with TypeScript to create functional and maintainable components. The type safety provided by TypeScript ensures that the components and hooks you create are reliable and bug-free. Experiment with other hooks and combinations to further explore React's capabilities.
