# Lab: Exploring Component Lifecycle in React with TypeScript

## Introduction

This lab is designed to give you a hands-on understanding of the component lifecycle in React applications using TypeScript. React class components have several lifecycle methods that are called at
different stages of a component's existence in the DOM. With TypeScript, you can ensure these methods are used correctly, providing additional safety and predictability to your React applications.

### Objectives

- Learn about the key lifecycle methods in React class components.
- Understand how to implement and use lifecycle methods in a React class component with TypeScript.

### Prerequisites

- Basic knowledge of React and TypeScript.
- Node.js installed on your system.
- A code editor, such as Visual Studio Code.

## Step 1: Setting Up Your Project

1. Create a new React project with TypeScript if you haven't done so already:

```bash
npx create-react-app my-lifecycle-lab --template typescript
cd my-lifecycle-lab
```

2. Open the project in your preferred code editor.

## Step 2: Creating a Class Component with Lifecycle Methods

1. Create a new file named `LifecycleComponent.tsx` in the `src` folder.

2. Inside `LifecycleComponent.tsx`, define a class component that includes several lifecycle methods:

```typescript
import React, {Component} from 'react';

interface Props {
}

interface State {
    time: Date;
}

class LifecycleComponent extends Component<Props, State> {
    timerID: number = 0;

    constructor(props: Props) {
        super(props);
        this.state = {time: new Date()};
    }

    componentDidMount() {
        this.timerID = window.setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: new Date()
        });
    }

    render() {
        return (
            <div>
                <h2>Current
        Time: {
            this.state.time.toLocaleTimeString()
        }
        </h2>
        < /div>
    )
        ;
    }
}

export default LifecycleComponent;
```

- `componentDidMount()`: Sets up a timer when the component is mounted.
- `componentWillUnmount()`: Clears the timer when the component is about to unmount.
- `tick()`: Updates the state with the current time every second.

## Step 3: Using the Component in Your Application

1. Open the `App.tsx` file.

2. Import the `LifecycleComponent` at the top of the file:

```typescript
import LifecycleComponent from './LifecycleComponent';
```

3. Use the `LifecycleComponent` within the `App` component:

```typescript
function App() {
    return (
        <div className = "App" >
        <header className = "App-header" >
            <LifecycleComponent / >
            </header>
            < /div>
    );
}

export default App;
```

## Step 4: Running the Application

1. Start the development server:

```bash
npm start
```

2. Open your web browser and navigate to `http://localhost:3000`. You should see the current time displayed, updating every second.

## Conclusion

You have now learned how to implement and use key lifecycle methods in React class components with TypeScript. Understanding these lifecycle methods allows you to perform specific actions at different
stages of a component's life in the DOM, such as setting up resources on component mount and cleaning them up on component unmount.

---

This lab provides a foundation for working with React class components' lifecycle methods. Experiment with other lifecycle methods, such as `shouldComponentUpdate`, `getDerivedStateFromProps`,
and `componentDidUpdate`, to see how they can be utilized in different scenarios to control component updates and behavior more precisely.