# Creating a React Project

In this lab, you will learn how to install Create React App (CRA) and use it to create your first React application. Create React App is a comfortable environment for learning React, and is the best
way to start building a new single-page application in React.

## Duration

20 minutes

## Step 1 - Setting Up Your Environment

- Ensure that Node.js and npm (Node Package Manager) are installed on your computer. If not, download and install them from [https://nodejs.org/](https://nodejs.org/).
- Create a new directory on your computer where you want to place your React project.
- Open a terminal and navigate to the directory you just created.

## Step 2 - Installing Create React App

Create React App (CRA) is a command-line tool that automates the setup of a new React application. It sets up your development environment so you can use the latest JavaScript features, provides a
nice developer experience, and optimizes your app for production.


## Step 3 - Creating a New React Application

1. **Create Your React App**
    - Use Create React App to create a new React application. Replace `my-react-app` with your desired project name.
      ```bash
      npx create-react-app my-react-app
      ```
    - Depending on your internet connection, this command may take a few minutes to complete. It will download the necessary files and dependencies to create your new React application.
    - This command creates a new directory named `my-react-app` (or your specified project name) with all the necessary files and configurations to start your React project.


2. **Navigate into Your Project Directory**
    - Once the installation is complete, navigate into your project directory.
      ```bash
      cd my-react-app
      ```

## Step 4 - Exploring Your React Application

1. **Structure of Your React Application**
    - Your new React application has a predefined structure. The `src` directory contains your React components and application logic, while the `public` directory contains static assets like HTML
      files and images.

2. **Available Scripts**
    - Inside your project directory, you can run several built-in commands:
        - `npm start` starts the development server and opens your app in the web browser.
        - `npm test` launches the test runner in the interactive watch mode.
        - `npm run build` builds the app for production to the `build` folder.

## Step 5 - Running Your React Application

1. **Start the Development Server**
    - Run the following command to start the development server. This command compiles your React application and opens it in your default web browser.
      ```bash
      npm start
      ```
    - After a few moments, your new React application should open in your web browser, displaying a welcome page.

## Well done! 👏

You've successfully installed Create React App and created your first React application. Explore the project structure and start building your own components to expand your application.