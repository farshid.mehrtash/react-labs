# Lab: Understanding State in React with TypeScript

## Introduction

This lab focuses on the concept of state in React applications using TypeScript. State is a crucial feature in React, allowing components to maintain and update their own data, leading to dynamic and
interactive user interfaces. TypeScript enhances state management by providing type safety, ensuring that the state and its updates are predictable and reliable.

### Objectives

- Learn how to define and use state in React functional components with TypeScript.
- Understand the benefits of type-checking state to ensure reliability and stability in your applications.

### Prerequisites

- Familiarity with React and TypeScript basics.
- Node.js installed on your system.
- A code editor, such as Visual Studio Code.

## Step 1: Setting Up Your Project

1. If you haven't already, create a new React project with TypeScript:

```bash
npx create-react-app my-state-lab --template typescript
cd my-state-lab
```

2. Open the project in your preferred code editor.

## Step 2: Creating a Typed Functional Component with State

1. Create a new file named `Counter.tsx` in the `src` folder.

2. Inside `Counter.tsx`, define a functional component that uses state to track a counter value. Use TypeScript to type the initial state:

```typescript
import React, {useState} from 'react';

const Counter: React.FC = () => {
    const [count, setCount] = useState<number>(0); // Typing the state

    const increment = () => {
        setCount(count + 1);
    };

    const decrement = () => {
        setCount(count - 1);
    };

    return (
        <div>
            <h2>Counter
:
    {
        count
    }
    </h2>
    < button
    onClick = {increment} > Increment < /button>
        < button
    onClick = {decrement} > Decrement < /button>
        < /div>
)
    ;
};

export default Counter;
```

- `useState<number>(0)` tells TypeScript that `count` is a number, ensuring type safety when updating and using this piece of state.

## Step 3: Using the Component with State

1. Open the `App.tsx` file.

2. Import the `Counter` component at the top of the file:

```typescript
import Counter from './Counter';
```

3. Use the `Counter` component within the `App` component:

```typescript
function App() {
    return (
        <div className = "App" >
        <header className = "App-header" >
            <Counter / >
            </header>
            < /div>
    );
}

export default App;
```

## Step 4: Running the Application

1. Start the development server:

```bash
npm start
```

2. Open your web browser and navigate to `http://localhost:3000`. You should see the counter component with increment and decrement buttons.

## Step 5: Exploring Complex State

State in React can be more than just numbers or strings; it can be objects or arrays. Let's explore using an object as state with TypeScript for type checking.

1. Modify `Counter.tsx` to use an object as state. This object will track both count and a text message:

```typescript
import React, {useState} from 'react';

interface CounterState {
    count: number;
    message: string;
}

const Counter: React.FC = () => {
    const [state, setState] = useState<CounterState>({count: 0, message: "You're at"}); // Typing the state

    const increment = () => {
        setState({...state, count: state.count + 1});
    };

    const decrement = () => {
        setState({...state, count: state.count - 1});
    };

    return (
        <div>
            <h2>{`${state.message} ${state.count}`
}
    </h2>
    < button
    onClick = {increment} > Increment < /button>
        < button
    onClick = {decrement} > Decrement < /button>
        < /div>
)
    ;
};

export default Counter;
```

- The `CounterState` interface defines the shape of the state, including a `count` number and a `message` string.
- When updating the state, we spread the current state (`...state`) to maintain other state properties, ensuring we only update the parts we want to.

## Conclusion

You've now learned how to manage state in React components using TypeScript for added type safety. This approach ensures that your stateful components are more predictable and easier to debug. By
understanding how to manage simple and complex state structures with TypeScript, you can build more robust and maintainable React applications.
