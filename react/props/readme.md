# Lab: Understanding Props in React with TypeScript

## Introduction

In this lab, we will explore the concept of props in React applications using TypeScript. Props are a way of passing data from parent components to child components, making them a crucial aspect of
React development for creating reusable and maintainable components. By integrating TypeScript, we gain the benefit of type checking, which enhances code reliability and developer productivity.

### Objectives

- Understand how to define and use props in React components with TypeScript.
- Learn to type-check props to improve code reliability.

### Prerequisites

- Basic knowledge of React and TypeScript.
- Node.js installed on your system.
- A code editor, such as Visual Studio Code.

## Step 1: Setting Up Your Project

1. Create a new React project with TypeScript:

```bash
npx create-react-app my-props-lab --template typescript
cd my-props-lab
```

2. Open the project in your preferred code editor.

## Step 2: Creating a Typed Functional Component

1. Create a new file named `Greeting.tsx` in the `src` folder.

2. Inside `Greeting.tsx`, define a functional component that accepts props for a greeting message. Use an interface to define the shape of the props:

```typescript
import React from 'react';

interface GreetingProps {
    message: string;
}

const Greeting: React.FC<GreetingProps> = ({message}) => {
    return <h1>{message} < /h1>;
};

export default Greeting;
```

- The `GreetingProps` interface specifies that the `Greeting` component expects a `message` prop of type `string`.
- `React.FC<GreetingProps>` is a generic type that makes the component aware of the `GreetingProps` structure.

## Step 3: Utilizing the Component with Props

1. Open the `App.tsx` file.

2. Import the `Greeting` component at the top of the file:

```typescript
import Greeting from './Greeting';
```

3. Use the `Greeting` component within the `App` component, providing a `message` prop:

```typescript
function App() {
    return (
        <div className = "App" >
        <header className = "App-header" >
        <Greeting message = "Hello, React with TypeScript!" / >
            </header>
            < /div>
    );
}

export default App;
```

## Step 4: Running the Application

1. Start the development server:

```bash
npm start
```

2. Open your web browser and navigate to `http://localhost:3000` to see the greeting message displayed.

## Step 5: Exploring Advanced Prop Types

TypeScript allows for more complex prop type definitions. Modify your `GreetingProps` interface to include optional props and props with specific types:

```typescript
interface GreetingProps {
    message: string;
    count?: number; // Optional prop
    isLoggedIn: boolean;
}
```

Update the `Greeting` component to use these props:

```typescript
const Greeting: React.FC<GreetingProps> = ({message, count = 0, isLoggedIn}) => {
    if (!isLoggedIn) {
        return <h1>Please
        log in to
        see
        the
        greeting. < /h1>;
    }

    return (
        <h1>
            {message}
    {
        count > 0 && `You have ${count} new messages.`
    }
    </h1>
)
    ;
};
```

Experiment with different ways of passing and using props to familiarize yourself with TypeScript's capabilities in React.

## Conclusion

You have now learned how to use props in React components with TypeScript for type-checking. This approach enhances code quality by ensuring that components receive and use props as intended. As you
become more comfortable with props and TypeScript, you'll find that your React applications become more scalable, maintainable, and easier to understand.
