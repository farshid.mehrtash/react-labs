# Creating a Hello World React Component

In this lab, you will learn how to create your first React component using TypeScript with Create React App (CRA). Understanding how to set up a React project with TypeScript, create type-safe
components, and utilize JSX syntax are crucial steps in building scalable and maintainable web applications.

## Duration

30 minutes

## Step 1 - Setting Up Your React Application

- Ensure you have Node.js and npm (Node Package Manager) installed on your computer. If not, download and install them from [https://nodejs.org/](https://nodejs.org/).
- Open a terminal and create a new React application with TypeScript support by running:
  ```bash
  npx create-react-app my-first-ts-react-component --template typescript
  ```
- Navigate to your new React application directory:
  ```bash
  cd my-first-ts-react-component
  ```

## Step 2 - Understanding the Project Structure

- Explore the generated project structure. Key directories and files include:
    - `public/`: Contains the static files for your project, such as `index.html`.
    - `src/`: Contains your React component files.
    - `src/App.tsx`: The main React component rendered by default, now with TypeScript.
    - `src/index.tsx`: The TypeScript entry point for React to mount the App component to the DOM.

## Step 3 - Creating Your First React Component

1. **Create a New Component File**
    - Inside the `src/` directory, create a new file named `Greeting.tsx`. This will be your custom React component.

2. **Writing the Component Code**
    - Open `Greeting.tsx` in your preferred text editor and implement the following React component structure using TypeScript:
      ```javascript
      import React from 'react';

      const Greeting: React.FC = () => {
        return <h1>Hello, React with TypeScript!</h1>;
      };
    
      export default Greeting;
      ```

## Step 4 - Using Your React Component

1. **Modify the App Component**
    - Open `src/App.tsx`. Import your `Greeting` component at the top of the file:
      ```javascript
      import Greeting from './Greeting';
      ```
    - Use the `Greeting` component within the `App` component's render method by adding `<Greeting />` inside the `return` statement. For example:
      ```javascript
      const App: React.FC = () => {
        return (
            <div className="App">
            <header className="App-header">
                <Greeting />
            </header>
            </div>
        );
      }
      ```

## Step 5 - Running Your Application

- Run your React application to see your new component in action:
  ```bash
  npm start
  ```
- Your default web browser should open automatically, displaying your React application with the `Greeting` component rendering "Hello, React!".

## Well done! 👏

You've successfully created and used your first React component within a Create React App project. This is a fundamental step towards building more complex and dynamic web applications using React.

## Files:

- [Greetings.tsx](Greeting.tsx)
- [App.tsx](App.tsx)