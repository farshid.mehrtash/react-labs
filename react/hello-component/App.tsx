import React from 'react';
import './App.css';
import Greeting from "./Greeting";

const App: React.FC = () => {
    return (
        <div className="App">
            <header className="App-header">
                <Greeting/>
            </header>
        </div>
    );
}

export default App;
