# React Fundamentals

**Introduction to React**

---

## What is React?

- An open-source JavaScript library for building user interfaces, particularly web applications with dynamic content.
- Allows developers to create large web applications that can change data, without reloading the page.
- The key to React's performance lies in its virtual DOM system, which optimizes updates to the web page by re-rendering only the components that change.

---

## Origin

- Developed by Facebook in 2011 and released to the public in 2013,
- React was born out of the company's need for a tool that could build dynamic and interactive user interfaces efficiently.
- Today, it is maintained by Meta and a community of individual developers and companies.

---

## Capabilities and Ecosystem

- React is known for its efficiency and flexibility.
- Reusibility, composable, and stateful is the key features of React.
- Vast array of tools and libraries, such as :
    - Redux for state management,
    - React Router for navigation,
    - Next.js for server-side rendering
    - ... and many more.

making it a comprehensive solution for web development

---

## React in Modern Web Development

React has become a cornerstone in modern web development due to its innovative approach to building user interfaces, its
comprehensive ecosystem, and its wide adoption by developers and companies alike.

It enables the creation of highly responsive and dynamic web applications, enhancing the user experience.

---

## Single Page Applications (SPA)

Single Page Applications (SPA) are web applications that load a single HTML page and dynamically update that page as the
user interacts with the app.

React plays a significant role in the development of SPAs by providing a powerful model for building fast and interactive user interfaces.

Its component-based architecture and efficient update mechanism make it an ideal choice for SPAs, contributing to smoother transitions and a more engaging user experience.

---

# Why React?

---

## Key Benefits

- Declarative
    - Allows developers to describe what they want their UI to look like, and React takes care of the how.
- Efficient
    - Utilizes a virtual DOM system to optimize updates and minimize direct manipulations of the actual DOM.
- Flexible
    - Can be integrated into existing projects or used to create new ones from scratch.

---

# The Virtual DOM

---

## What It Is and Why It Matters

The Virtual DOM is a lightweight copy of the actual DOM (Document Object Model).

It allows React to perform updates more efficiently by minimizing direct
manipulations of the DOM, **which can be slow**.

When a component’s state changes, React first updates the Virtual DOM, then compares the updated Virtual DOM with a
pre-update version, and finally applies only the actual changes to the real DOM.

Known as **diffing**, significantly improves application performance.

---

## Reusable Components

React promotes the creation of

- independent
- reusable

components that manages its own state.

This modular approach makes development faster,

**Why?**

- developers can reuse components across different parts of an application or even in entirely different projects.
- It also simplifies maintenance and testing,
    - Leads to more stable and reliable code.

---

## Community Support

- A vast array of libraries, tools, and frameworks are available to extend React’s
  capabilities,

- Covering everything from state management (e.g., Redux, MobX) to routing (React Router) and beyond.
    - Documentation
    - Tutorials
    - Forums
    - Third-party plugins

---

## Who is using it?

Major companies and organizations:

- Facebook (Meta)
- Instagram,
- Netflix
- Airbnb
- Uber
- GotPhoto

---

# React vs. jQuery

**Understanding the Differences**

---

## jQuery's DOM

It directly manipulates the actual DOM, making it straightforward for tasks like DOM manipulation and event handling

---

## React VDOM

React uses a virtual DOM that offers a more efficient way of updating the user interface by minimizing direct DOM manipulation,

leads to better performance for dynamic applications

---

## The Shift from Imperative to Declarative Programming

jQuery uses an imperative approach

- The developer needs to explicitly tell the browser how to perform tasks step by step.
    - This can make complex applications hard to manage and scale.
- React, uses declarative paradigm,
    - Developers to describe the desired outcome and letting React take care of the how.
        - Cleaner
        - Readable code that's easier to debug.

---

## Performance Considerations and Scalability

While jQuery can be more than adequate for smaller projects or simpler web pages, its direct DOM manipulation becomes less efficient as the project's complexity
grows.

React's virtual DOM and component-based architecture make it significantly more efficient and scalable for larger, more complex applications, offering
smoother updates and better overall performance.

---

# Use Cases

**Transitioning to React**

---

## jQuery

Best suited for small to medium websites, adding interactivity to web pages, and when working on projects where full-fledged frameworks like React may not be necessary.

---

## React

Ideal for developing large-scale applications with complex user interfaces, Single Page Applications (SPAs), and when you need a scalable,
maintainable codebase.

---

# The React Ecosystem

---

## Overview

The React ecosystem is vast and varied, offering numerous tools and libraries that enhance React's capabilities and streamline the development process.

---

## Redux

A state management library that provides a predictable state container for JavaScript apps, making it easier to manage the state across the entire application.

---

## React Router

A library for routing in React applications, enabling the navigation among different components within a React application, with URL synchronization.

---

## Next.js

A React framework for server-side rendering, static site generation, and creating web applications with optimized performance out of the box.


---

## Design Libraries

- **Material-UI**
- **Ant Design**,
- **Tailwind CSS**
- **React Bootstrap**

and other UI libraries: Offer pre-built React components that accelerate the development of attractive and responsive user interfaces.

---

## Node.js

- Node.js is the runtime environment for executing JavaScript code server-side.
- It is essential for developing, testing, and running React applications.

---

## NPM

- Comes with Node.js
- It Is vital for managing packages and dependencies in Node.js projects.
- It allows developers to install, share, and manage libraries and tools within their project

---

## Yarn

- A package manager that is compatible with the NPM registry and offers faster and more efficient package installation.
- It is particularly useful for large-scale projects with many dependencies.

---


## PNPM

- PNPM stands for performant npm. 

- It's a fast, efficient package manager for Node.js that addresses some of the shortcomings of npm and Yarn, particularly in terms of speed and disk space usage. 

_How?_ By creating a single, shared `node_modules` folder for all projects, 

- PNPM drastically reduces the amount of disk space required for project dependencies.

---

## NPX

- A package runner tool that comes with NPM.
- It allows you to execute packages directly from the NPM registry without having to install them globally.
- Useful for running packages for one-off tasks or for trying out new tools and libraries.

---

## MonoRepo

A MonoRepo, short for **monolithic repository**, is a version control strategy that involves storing multiple projects in a single repository. 

This approach is contrasted with a multi-repo setup, where each project or module has its own repository. 

Monorepos can contain various types of projects, including libraries, services, and applications, potentially spanning across different technologies and frameworks.

---
## Example of a MonoRepo Structure

```
/my-monorepo
    /projects
        /app1
        /app2
        /library1
        /library2
    /packages
        /shared-ui-components
        /utils
    /scripts
        /build
        /test
```

---

## Create React App Tool

- Create React App is an officially supported way to create single-page React applications.

- It offers a modern build setup with no configuration needed.

---

## Storybook

Storybook is an open-source tool that provides a development environment for UI components.

It supports multiple frameworks including React, Vue, and Angular.

Enables developers to build and test UI components in isolation, outside their applications,

Facilitates a more focused and efficient component development process.

---

## Storybook in Development

To use Storybook, you integrate it into your project as a development dependency.

Once installed, you can run Storybook locally to view your component library in a web-based UI.

_Developers write "stories" in JavaScript or TypeScript to represent the various states of their UI components._

---

## Jest

Jest is a JavaScript testing framework maintained by Facebook, designed with simplicity and support for large web applications in mind. 

Jest is particularly well-suited for testing React applications, but it can be used with any JavaScript code.

---

### Key Features of Jest

- **Zero Configuration**: Jest is designed to work out of the box with minimal setup, making it ideal for new projects or for developers who want to avoid extensive configuration.
- **Snapshot Testing**: Jest introduced the concept of snapshot testing, which captures the output of a component and ensures it doesn't change unexpectedly in future tests.
- **Built-in Code Coverage**: Jest includes a built-in code coverage tool, making it easy to see how well your codebase is tested without needing to set up additional tools.
- **Isolated and Fast**: Tests run in parallel in their own processes to maximize performance. Jest's caching mechanisms also help in running tests faster by only running tests related to changed files.
- **Mocking Support**: Jest has extensive support for mocks, allowing developers to write tests for modules or functions that depend on external services or modules with ease.

---


# Recap

---

## Speed

React's use of the Virtual DOM is a key factor in its performance. 

By minimizing direct DOM manipulation, React ensures faster UI updates, which is critical for
creating smooth, dynamic user experiences. This approach allows React applications to handle complex updates with minimal performance impact.
---

## Flexibility

It can be easily integrated into existing projects or used in combination with other libraries and frameworks. 

---

## Scalability

React's component-based architecture not only makes code more reusable and easier to maintain but also supports scalability. 


---

## Community

The React community is vast and active, offering a wealth of libraries, tools, and resources that extend React's capabilities. 

---

## Future-Proof

React continues to evolve, with ongoing updates and support from both Facebook (Meta) and a large community of developers. 

---


Story is over but the journey has only just begun.

[Next: ES6 ](./es6.md)


