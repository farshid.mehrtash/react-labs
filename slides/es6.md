# Introduction to ES6

---

## What is ES6/ECMAScript 2016?

ECMAScript 2016, also recognized as a major revision to the JavaScript programming language,

Introduces numerous new syntaxes and features aimed at enhancing the efficiency, simplicity, and readability of JavaScript code, building on the foundational scripting language
specification that JavaScript adheres to.


---

## Why ES6 is a Game-Changer for JavaScript

- Addressed common pain points and limitations of earlier JavaScript versions.
- ES6 made JavaScript more powerful and flexible
- It also improved readability and maintainability of the code, making it easier for developers to work on large-scale projects.

---

## NodeJs

NodeJS is a JavaScript runtime built on Chrome's V8 JavaScript engine, allowing you to run JavaScript on the server side.

---

# Key Features of ES6

---

## let/const

These declarations provide block-scope variables and constants.

Offers more precise control over variable scope and reducing errors due to variable hoisting seen with the `var` keyword.

---

## `let`

allows you to declare variables that are limited in scope to the block, statement, or expression in which they are used, unlike `var`, which
declares a variable globally or locally to an entire function regardless of block scope.

---

## Example of `let`

```
function varVsLetExample() {
  if (true) {
    var varVariable = "I am defined using var!";
    let letVariable = "I am defined using let!";
  }

  console.log(varVariable); // Output: "I am defined using var!"
  // This works because varVariable is function-scoped and accessible here.

  try {
    console.log(letVariable); // This will cause an error.
  } catch (e) {
    console.log("letVariable is not accessible here."); // Output: "letVariable is not accessible here."
    // This error occurs because letVariable is block-scoped due to 'let' and not accessible outside the if block.
  }
}

varVsLetExample();
```

---

## `const`

Is used to declare constants which are block-scoped much like variables declared using `let`, but their value cannot be reassigned after they are
declared.
---

## Example of `const`

```
function constExample() {
  const greeting = "Hello, world!";
  console.log(greeting); // Output: "Hello, world!"

  // Trying to reassign a value to the greeting constant will result in an error.
  try {
    greeting = "Hello, again!";
  } catch (e) {
    console.log("Cannot reassign a value to a constant."); // Output: "Cannot reassign a value to a constant."
  }

  // Demonstrating block scope
  if (true) {
    const blockScopedVariable = "I am inside a block!";
    console.log(blockScopedVariable); // Output: "I am inside a block!"
  }

  try {
    console.log(blockScopedVariable); // This will cause an error.
  } catch (e) {
    console.log("blockScopedVariable is not accessible here."); // Output: "blockScopedVariable is not accessible here."
    // This error occurs because blockScopedVariable, declared with 'const', is block-scoped and not accessible outside the if block.
  }
}

constExample();

```

---

## Classes

Although React primarily uses functions for components, ES6 classes can be used to define components, especially when working with more complex
stateful components and lifecycle methods.

---

## Example of Classes

```
class Counter extends React.Component {
  constructor(props) {
    super(props);
    // Initial state
    this.state = { count: 0 };
  }

  // Method to increment the count
  incrementCount = () => {
    this.setState({ count: this.state.count + 1 });
  };

  render() {
    return (
      <div>
        <p>Count: {this.state.count}</p>
        <button onClick={this.incrementCount}>Increment</button>
      </div>
    );
  }
}
```

---

## Arrow Functions

Arrow functions let you write shorter functions without their own `this`.

They take `this` from where they were made, not where they are used.

This is great for quick functions and when you need to use `this` from outside the function.

---

## Example of Arrow Functions

```
class MessageDisplay {
  constructor() {
    this.message = "Hello, world!";
  }

  // Regular function
  showMessageWithFunction() {
    setTimeout(function() {
      console.log(this.message); // 'this' refers to the function's own context, which does not have a 'message' property, so this will be undefined.
    }, 1000);
  }

  // Arrow function
  showMessageWithArrow() {
    setTimeout(() => {
      console.log(this.message); // 'this' is inherited from the surrounding scope, i.e., the MessageDisplay instance, so it correctly accesses 'message'.
    }, 1000);
  }
}

const display = new MessageDisplay();
display.showMessageWithFunction(); // Likely undefined or throws an error in strict mode.
display.showMessageWithArrow(); // Correctly logs "Hello, world!"

```

---

## Template Literals

Template literals are string literals allowing embedded expressions.

Enclosed by backticks (`` ` ``), they can contain placeholders marked by `${expression}` syntax.

This feature makes constructing strings easier and more intuitive, especially when dynamically inserting variables or expressions.

---

## Example of Template Literals

```
const user = {
  name: 'John Doe',
  age: 30
};

// Using template literals to include variables and expressions
const greeting = `Hello, my name is ${user.name} and I will be ${user.age + 1} years old next year.`;

console.log(greeting);
// Output: "Hello, my name is John Doe and I will be 31 years old next year."

```

---

## Destructuring

This feature allows for easier extraction of properties from objects and arrays, which is particularly handy when working with props and
state in React components.


---

## Example of Destructuring

```
function UserProfile({ userData }) {
  // Object destructuring to extract properties from the userData object
  const { name, age, location } = userData;

  return (
    <div>
      <h1>User Profile</h1>
      <p>Name: {name}</p>
      <p>Age: {age}</p>
      <p>Location: {location}</p>
    </div>
  );
}

// Example usage of the UserProfile component with a userData prop
const userData = {
  name: 'Jane Doe',
  age: 28,
  location: 'New York'
};

<UserProfile userData={userData} />

```

---

## Default Parameters, Spread Operator, and Rest Parameters

These features provide more flexible ways to handle functions parameters, making functions easier to use and more adaptable to different inputs.

---

## Example of Default Parameters

```
// Default parameters allow you to set default values for function parameters
function greet(name = 'Guest') {
  console.log(`Hello, ${name}!`);
}

greet(); // Output: Hello, Guest!
greet('Anna'); // Output: Hello, Anna!

// The spread operator allows you to expand elements of an iterable (like an array) into individual arguments of a function
function sum(x, y, z) {
  return x + y + z;
}

const numbers = [1, 2, 3];
console.log(sum(...numbers)); // Output: 6

// Rest parameters allow you to represent an indefinite number of arguments as an array
function multiply(multiplier, ...theArgs) {
  return theArgs.map(x => multiplier * x);
}

console.log(multiply(2, 1, 2, 3)); // Output: [2, 4, 6]

```

---

## Short explanation of the spread operator

- When `multiply(2, 1, 2, 3)` is called, `multiplier` is assigned the value `2`, and `theArgs` collects the remaining arguments into an array, so `theArgs` equals `[1, 2, 3]`.
- Inside the function, `theArgs.map(x => multiplier * x)` iterates over each element in `theArgs`, multiplies it by the `multiplier`, and returns a new array with the results.
  The `map` function is used here for array transformation.
- For each element `x` in the array `theArgs`, it's multiplied by `multiplier` (which is `2` in this case), resulting in a new array `[2, 4, 6]` where each element is the product
  of the original element and the multiplier.
  e a flexible way to handle functions that can accept any number of arguments. By collecting arguments into an array, they make it easy to perform operations like mapping,
  filtering, or reducing over all the arguments without knowing in advance how many arguments will be passed.

---

## Modules

ES6 introduced a more standardized and modular approach to organizing and importing/exporting code, making it easier to manage and reuse code across

---

## Example of Modules - P1

```
// mathUtils.js

// Function to add two numbers
export function add(a, b) {
  return a + b;
}

// Function to subtract two numbers
export function subtract(a, b) {
  return a - b;
}
```

---

## Example of Modules - P2

```
// app.js

import { add, subtract } from './mathUtils.js';

console.log(add(2, 3)); // Output: 5
console.log(subtract(5, 2)); // Output: 3

```

---

## Promises

Promises are a more flexible and powerful alternative to callbacks for handling asynchronous operations in JavaScript.

They simplify the process of working with asynchronous code, making it easier to manage and reason about asynchronous operations.

---

# Example of Promises

---

## Without Promises

```
function fetchUserData(userId, callback) {
  // Simulating an API call
  setTimeout(() => {
    const userData = { id: userId, name: "John Doe" }; // Simulated response
    callback(userData);
  }, 1000); // Simulates network delay
}

fetchUserData(1, function(userData) {
  console.log(userData); // Output: { id: 1, name: "John Doe" }
});
```

---

## With Promises

```
function fetchUserData(userId) {
  // Returning a promise
  return new Promise((resolve, reject) => {
    // Simulating an API call
    setTimeout(() => {
      const userData = { id: userId, name: "John Doe" }; // Simulated response
      resolve(userData); // Resolving the promise with the user data
    }, 1000); // Simulates network delay
  });
}

fetchUserData(1)
  .then(userData => {
    console.log(userData); // Output: { id: 1, name: "John Doe" }
  })
  .catch(error => {
    console.error(error); // Handling errors
  });

```

---

## Tip of the Iceberg

These ES6 features represent just a fraction of the improvements made to JavaScript with the ES6 update, but they are among the most impactful for developers.

---

# Exploring JSX

---

## Definition

JSX stands for JavaScript XML.

It is a syntax extension for JavaScript, used with React to describe what the UI should look like.

By using JSX, you can write HTML structures in the same file as your JavaScript code, making the development process more efficient and intuitive.

---

## How JSX Simplifies React Component Development

JSX simplifies React component development by allowing developers to write HTML in JavaScript.

This integration provides a clear and concise way to create UI components, enhancing readability and maintainability.

It makes it easier to visualize the UI, debug code, and detect errors, leading to a smoother development experience.

---

## Benefits

The primary benefit of writing HTML within JavaScript via JSX is the ability to leverage the full power of JavaScript within your markup.

JSX supports JavaScript expressions, which are written inside curly braces `{}`.

This means you can dynamically generate content, handle events, and react to state and props changes within the same snippet of code.

---

## Example

A Simple JSX Snippet and Its Compiled JavaScript Output

**JSX Snippet:**

```
const element = <h1>Hello, world!</h1>;
```

**Compiled JavaScript Output:**

```javascript
const element = React.createElement('h1', null, 'Hello, world!');
```

This example demonstrates how JSX abstracts away the complexity of creating React elements with `React.createElement()`, offering a more accessible syntax for
developers.

---

# JSX Best Practices

---

## Embedding Expressions

You can embed any JavaScript expression in JSX by wrapping it in curly braces.

This is useful for dynamically updating your UI based on state or props.

Example:

```
<div>{user.isLoggedIn ? 'Logout' : 'Login'}</div>
```

---

## Specifying Attributes

Attributes in JSX are often specified with camelCase property naming convention.

For attributes, you can assign strings directly or use expressions for dynamic values.

Example:

```
function CustomButton({ buttonText }) {
  return (
    <button style={{ backgroundColor: 'blue', color: 'white', padding: '10px 20px', borderRadius: '5px' }}>
      {buttonText}
    </button>
  );
}
```

---

## Child Elements

JSX tags can contain child elements, allowing you to compose complex UIs.

If a tag is empty, you can close it immediately with `/>`.

Example:

```
function WelcomeMessage() {
  return (
    <div>
      <h1>Welcome to Our Website</h1>
      <p>This is a place where you can learn more about us and what we do.</p>
      <img src="logo.png" alt="Our Logo" />
    </div>
  );
}
```

---

## Step-by-Step Guide to Installing NodeJS and npm on Your System

1. **Download NodeJS**: Visit [the official NodeJS website](https://nodejs.org/) and download the version recommended for most users. This version includes both
   NodeJS and npm.
2. **Install NodeJS and npm**: Run the downloaded installer, following the prompts to install both NodeJS and npm on your system.
3. **Verify Installation**: Open your terminal or command prompt and run `node -v` and `npm -v` to verify the successful installation of NodeJS and npm,
   respectively. You should see the installed versions displayed.

---

# Package File

---

## The Heart

`package.json` is a fundamental file in any Node.js project or npm package.

It serves as a project manifest, containing metadata about the project, such as its name, version, and dependencies.

This file is used by npm to identify the project and manage its dependencies.

---

# Key Sections

---

## `scripts`

Defines commands that can be executed on the project.

Common scripts include

- `start` for launching the application
- `test` for running tests,
- `build` for building the project.

---

## `dependencies`

Lists the libraries and packages your project needs to run.

These are installed in the `node_modules` folder and are required for the application to work in production.

---

## devDependencies

Similar to `dependencies`, but includes packages needed only for development and testing, not in production.

This can include testing frameworks, build tools, and compilers.

---

## `package-lock.json`

`package-lock.json` is automatically generated when you install npm packages.

It locks the versions of all packages and their dependencies, ensuring that every install results in the exact same file structure in `node_modules` across all environments.

This file helps to mitigate issues arising from version mismatches and enhances project stability.

---

# Managing Packages

**with `npm`**

---

## Installing Packages

- `npm install <package-name>`
    - to add a new package to your project.

- Adding `--save` will add the package to `dependencies`,
- Adding `--save-dev` will add it to `devDependencies`.

---

## Updating Packages

- `npm update <package-name>` updates a package to its latest version based on the version range specified in `package.json`.

---

## Removing Packages

- `npm uninstall <package-name>` removes a package from your project and also its entry from `package.json`.

---

## Understanding npm Scripts for Task Automation

npm scripts are an essential part of project automation, allowing you to automate repetitive tasks such as building, testing, and deployment. 

By defining these tasks in the `scripts` section of `package.json`, you can execute them with `npm run <script-name>`. 

This feature significantly streamlines the development process, making it more efficient and error-resistant.

---

# Typescript

---

## What is TypeScript?

TypeScript is a superset of JavaScript that adds static types to the language.

It is designed for large-scale applications and transpiles to plain JavaScript.

---


## Key Features of TypeScript

- **Static Typing**: TypeScript adds static types to JavaScript, allowing for type checking at compile time.
- **Type Inference**: TypeScript can infer types based on the context, reducing the need for explicit type annotations.
- **Interfaces**: TypeScript supports interfaces for defining object shapes and contracts.
- **Enums**: TypeScript provides support for enums, allowing you to define a set of named constants.
- **Classes**: TypeScript supports class-based object-oriented programming, including inheritance and access modifiers.
- **Generics**: TypeScript supports generics, enabling the creation of reusable components with dynamic types.
- **Decorators**: TypeScript provides decorators for adding metadata and behavior to classes and class members.

---

## tsconfig.json

The `tsconfig.json` file is used to configure the TypeScript compiler and specify the root files and compiler options for a TypeScript project.

This file is essential for setting up a TypeScript project and customizing the compiler behavior to suit your needs.

---

## Example of a `tsconfig.json` File

```json
{
  "compilerOptions": {
    "target": "es6",                     // Specify ECMAScript target version
    "module": "commonjs",                // Specify module code generation
    "strict": true,                      // Enable all strict type-checking options
    "esModuleInterop": true,             // Enables interoperability between CommonJS and ES Modules
    "skipLibCheck": true,                // Skip type checking of declaration files
    "outDir": "./dist"                   // Redirect output structure to the directory
  },
  "include": ["src/**/*"],               // Specify files to be included
  "exclude": ["node_modules"]            // Specify files to be excluded
}
```

---

# Lab

**Hello World with NodeJs and ES6**

---

The end of this presentation marks the beginning of our real-world journey.

[Next: React Component](./react-componenet.md)
