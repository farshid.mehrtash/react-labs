# React Components

---

# Lab

**Installing Create React App (CRA) and Creating Your First React Application**

---

## Key Files

Upon creating a new app with CRA, you'll find the following key files and directories:

- `public/index.html` - The single HTML file used by your React app. This file is the entry point for your application and is where you can include other
  resources such as stylesheets and scripts.
- `src/index.js` - The JavaScript entry point for your React app. This file is where you import your components and render them to the DOM.
- `src/App.js` - A basic React component serving as the starting point for your application.

These files provide the foundation for building your React application.

---

## Development Server

1. **Start the Development Server**: Inside your project directory, run `npm start`. This command starts the development server and opens your React application
   in the default web browser.
2. **Development Server Features**: The development server provides live reloading, meaning it will automatically refresh your app in the browser as you make
   changes to the code.
3. **Viewing Your App**: With the development server running, you can view your app by visiting `http://localhost:3000` in your browser.

**Remember:** This server is not suitable for production use, but it's great for development and testing.

---

## React Components

React components are the core building blocks of a React application.

They enable you to split the UI into independent, reusable pieces that can be handled separately.

Each component has its own properties and state, allowing for complex applications to be built out of simple elements.

---

# Functional vs. Class Components

---

## Functional Components

These are JavaScript functions that return HTML (via JSX). They accept props as an argument and return React elements. With the
  introduction of Hooks, functional components can now manage state and side effects, making them more powerful and often the preferred choice for new
  components.

---

## Class Components

Before Hooks, class components were the only way to include state and lifecycle methods in your components. They extend
  from `React.Component` and require a render function that returns HTML.

---

## Choosing

The choice between functional and class components often comes down to personal preference and the specific needs of your project.

However, functional components are now recommended for their simplicity and the power of Hooks.

---

# Lab

**Creating Your First React Component**

---

# States and Props

---

That is a story for another day.

See you in the next session.

