# React-Labs

## Lab Environment

You can use your personal or work computer to complete the labs in this course. The following software is required:
- [Node.js](https://nodejs.org/)
- [WebStorm](https://www.jetbrains.com/webstorm/) or [Visual Studio Code](https://code.visualstudio.com/)

## Slides

You can find the slides for this course in the [slides](slides) directory.

## Lab Files

- [Setting Up a TypeScript Project](nodejs/readme.md)
- [Creating a React Project](react/basic/readme.md)
- [Creating a Hello World React Component](react/hello-component/readme.md)