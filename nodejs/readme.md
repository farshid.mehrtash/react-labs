# Setting Up a TypeScript Project

In this lab, you will learn how to set up a TypeScript project, configure it to use Node.js, and write a simple introduction project utilizing TypeScript features along with ES6 features
like `const`/`let`, destructuring, and the import/export syntax.

## Duration

40 minutes

## Step 1 - Preparing Your Environment

- Ensure Node.js is installed on your computer. If not, download and install it from [https://nodejs.org/](https://nodejs.org/).
- Create a new directory for your project on your computer, such as `my-typescript-project`.
- Open a terminal and navigate to your project directory.
  ```bash
  cd path/to/my-typescript-project
  ```
- Initialize a new Node.js project by running:
  ```bash
  npm init
  ```

This command prompts you to enter information about your project, such as the project name, version, description, and entry point. You can press `Enter` to accept the default values for most of the
prompts.

This command creates a `package.json` file with default values.

## Step 2 - Setting Up TypeScript

1. **Install TypeScript and Node.js Types**
    - Install TypeScript and the Node.js types for development by running:
      ```bash
      npm install --save-dev typescript
      npm install --save-dev @types/node
      ```

2. **Initialize a TypeScript Configuration File**
    - Generate a `tsconfig.json` file by running:
      ```bash
      tsc --init
      ```
    - Open `tsconfig.json` and ensure `"module": "commonjs"` and `"target": "es6"` are set to utilize commonjs modules and ES6 features.

3. **Creating Your Main Script File**
    - Create a new file named `index.ts`. This will serve as the entry point of your application.

## Step 3 - Writing Your TypeScript Script

1. **Using `const` and `let` with TypeScript**
    - In `index.ts`, declare a constant and a variable using `const` and `let`.
      ```typescript
      const message: string = 'Hello, Node.js!';
      let count: number = 0;
      ```

2. **Implementing Destructuring**
    - Use destructuring to extract values from an object.
      ```typescript
      const person: { name: string; age: number } = { name: 'John Doe', age: 30 };
      const { name, age } = person;
      console.log(name, age); // Output: John Doe 30
      ```

3. **Creating and Exporting a Module**
    - Create another file named `greetings.ts`. Write a function that returns a greeting message and export it using TypeScript syntax.
      ```typescript
      // greetings.ts
      export const greetingMessage = (name: string): string => `Hello, ${name}!`;
      ```

4. **Importing and Using Your Module**
    - Back in `index.ts`, import the module you just created and use it.
      ```typescript
      // index.ts
      import { greetingMessage } from './greetings';
      console.log(greetingMessage('TypeScript User')); // Output: Hello, TypeScript User!
      ```

## Step 4 - Compiling Your TypeScript Project

- Compile your TypeScript files to JavaScript by running:
  ```bash
  tsc
  ```
- This command compiles your `.ts` files based on the configuration specified in `tsconfig.json`.

## Step 5 - Running Your Compiled JavaScript

- Execute the compiled JavaScript file by running:
  ```bash
  node index.js
  ```
- You should see the greeting message output to the terminal.

## Well done! 👏

You've successfully set up a TypeScript project, configured it for Node.js, and written a simple introduction project utilizing TypeScript along with modern JavaScript features.