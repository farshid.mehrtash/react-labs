const message: string = 'Hello, Node.js!';
let count: number = 0;

console.log(message);
console.log(count);

// Try to change the value of message and count and see the output.


const person: { name: string; age: number } = {name: 'John Doe', age: 30};
const {name, age} = person;
console.log(name, age); // Output: John Doe 30

import {greetingMessage} from './greetings';

console.log(greetingMessage('TypeScript User')); // Output: Hello, TypeScript User!